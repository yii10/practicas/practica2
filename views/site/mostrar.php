<?php

use yii\grid\GridView;

?>

<?= 
    GridView::widget([
        'dataProvider'=>$datos,
        'summary'=>"Mostrando {begin} - {end} de {totalCount} elementos",
        'columns'=> [
            ['class'=>'yii\grid\SerialColumn'],
            'id',
            'texto'
        ],
    ]);
?>
